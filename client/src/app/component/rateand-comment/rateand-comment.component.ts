import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RequestService } from '../../service/request.service';
import { UserService } from '../../service/user.service';
@Component({
  selector: 'app-rateand-comment',
  templateUrl: './rateand-comment.component.html',
  styleUrls: ['./rateand-comment.component.scss']
})
export class RateandCommentComponent {
  comment: String;
  target: String;
  jobID: String;
  communication: Number= 3;
  punctuality: Number= 3;
  professionalism: Number= 3;

  constructor(private route: ActivatedRoute,
      private requestService: RequestService,
      private userService: UserService,
      private router: Router) {
    this.route.params.subscribe( params => {
      this.target = params['mail'];
      this.jobID = params['job'];
      console.log(this.target);
    });
  }

  back(){
    this.router.navigateByUrl('/home');
  }

  onComRatingChange(event) {
    this.communication = event.rating;
  }

  onPunctatingChange(event) {
    this.punctuality = event.rating;
  }

  onProRatingChange(event) {
    this.professionalism = event.rating;
  }

  SendRate(){
    this.requestService.request('user/rate', {
      token: localStorage['usertoken'],
			target: this.target,
      j: this.jobID,
			communication: this.communication,
      punctuality: this.punctuality,
      professionalism: this.professionalism,
			comment: this.comment
    }, data => this.router.navigateByUrl('/home'));
  }

}
