import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RateandCommentComponent } from './rateand-comment.component';

describe('RateandCommentComponent', () => {
  let component: RateandCommentComponent;
  let fixture: ComponentFixture<RateandCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RateandCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateandCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
