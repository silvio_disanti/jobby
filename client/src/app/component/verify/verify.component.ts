import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RequestService } from '../../service/request.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})
export class VerifyComponent {
  username: String;
  verified: boolean = false;
  email = localStorage['email'];

  constructor(private route: ActivatedRoute, private requestService: RequestService, private router: Router) {
    this.route.params.subscribe( params => {
      if (params['id'] && params['id'] != '')
        this.requestService.request('auth/verify',
          {id: params['id']},
          res => {
            this.verified = true;
            this.username= res;
            localStorage.removeItem('email');
          },
          err => console.log('verifica non effettuata')
        );
    });
  }

  back(){
    this.router.navigateByUrl('/register');
  }

  andiamo(){
    this.router.navigateByUrl('/login');
  }

}
