import { Component, OnInit, Input } from '@angular/core';
import { RequestService } from '../../service/request.service';
import { AlertService } from '../../service/alert.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-registrazione',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  username;
  pwd;
  pwd2;
  phone_number;
	email;
	vat;
  country;
  zipcode;
  province;
  city;
  address;
  house_numbering;
  countryTree: String[] = ['IT', 'FR', 'GB'];
  privateUser = true;
  hide = true;

	constructor(
    private requestService: RequestService,
    private alertService: AlertService,
    private router: Router
){}
	register() {
    this.requestService.request('auth/register', {
	     username: this.username,
	     password: this.pwd,
	     vat: this.vat,
	     email: this.email,
	     phone_number: this.phone_number,
       country: this.country,
       zipcode: this.zipcode,
       province: this.province,
       city: this.city,
       address: this.address,
       house_numbering: this.house_numbering
     },
     (data) => {
       localStorage.setItem('email', this.email);
       console.log(localStorage['email']);
       this.router.navigateByUrl('/verify');
     }, (error) => this.alertService.error(error.error)
   );
  }

  enableVat() {
    this.privateUser = false;
  }

  disableVat() {
    this.privateUser = true;
  }

  ngOnInit() {
  }

}
