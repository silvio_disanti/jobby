import { Component, OnInit } from '@angular/core';
import { Job } from '../../model/job';
import { UserService } from '../../service/user.service';
import { JobService } from '../../service/job.service';

@Component({
  selector: 'app-assigned-to-me',
  templateUrl: './assigned-to-me.component.html',
  styleUrls: ['./assigned-to-me.component.scss']
})
export class AssignedToMeComponent implements OnInit {
   jobs: Job[] = this.jobService.onlyMyAssignedJobs();

  constructor(private jobService: JobService, private userService: UserService) { }

  ngOnInit() {
    let assignJobs = u => this.jobs = this.jobService.onlyMyAssignedJobs();
    this.userService.getObservable().subscribe(assignJobs);
    this.jobService.getObservable().subscribe(assignJobs);
  }

}
