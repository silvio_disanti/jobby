import { Component, Input, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { UserService } from '../../service/user.service';
import { RequestService } from '../../service/request.service';
import { AlertService } from '../../service/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent {

	email: string;
	password: string;

  hide = true;

	constructor(
		private requestService: RequestService,
		private userService: UserService,
		private alertService: AlertService,
    private router: Router
    ){ }

	login(){
    this.requestService.request('auth/login', {
			email: this.email,
			password: this.password
    }, (data) => {
      localStorage.setItem('usertoken', data.token);
      //console.log(data);
      this.userService.setUser();
      this.router.navigateByUrl('/home');
    }, err => this.alertService.error(err.error));
	}
}
