import { Component, Input } from '@angular/core';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

import { UserService } from '../../service/user.service';
import { RequestService } from '../../service/request.service';
import { Utils } from '../../utils';
import { User } from '../../model/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {
  private mail;

  private user: User = new User();
  private editedUser: User;
  private countryTree: String[] = ['IT', 'FR', 'GB'];
  private loaded_image: String;
  private formData = new FormData();
  hide = true;

  canModify = true;
  modifyInformation = false;

  newUsername;
  newPhone;
  newPassword;
  newVat;

  constructor(private router: Router,
    private userService: UserService,
    private requestService: RequestService,
    private route: ActivatedRoute) {
      this.userService.getObservable().subscribe(user => {
        this.canModify = this.mail == user.email;
      });
      this.route.params.subscribe( params => {
        this.mail = params['mail'];
        this.canModify = this.mail == this.userService.getUser().email;
        if (!this.canModify)
          this.userService.getSpecificUser(params['mail'], user => this.user = user);
        else
          this.user = this.userService.getUser();
      });
    }

    modify() {
      this.modifyInformation = true;
      this.editedUser = JSON.parse(JSON.stringify(this.user));
      this.loaded_image = undefined;
    }

    cancelModify() {
      this.modifyInformation = false;
    }

    modifyUser() {
      this.requestService.request('user/modifyUser',{
        token: localStorage['usertoken'],
        updatePassword: this.newPassword,
        updated_user: this.editedUser,
        formData:this.formData
      }, (res) => {
        this.userService.setUser();
        if (this.loaded_image) this.uploadProfileImage();
        else this.router.navigateByUrl('/home');
      });
    }

    uploadProfileImage() {
      this.requestService.request('user/profileImage',this.formData,
        result => this.router.navigateByUrl('/home')
      );
    }

    profileImage(event) {
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();

        reader.readAsDataURL(event.target.files[0]);
        reader.onload = e => this.loaded_image = reader.result;


        this.formData.append('file', event.target.files[0], this.user.email);
      }
    }

    getAddress() {
      return Utils.AddressToString(this.user.address_information);
    }

    logout() {
      this.userService.clearUser();
    }
}
