import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RequestService } from '../../service/request.service';
import { Router } from '@angular/router';
import { AlertService } from '../../service/alert.service';

@Component({
  selector: 'app-password-recovery',
  templateUrl: './password-recovery.component.html',
  styleUrls: ['./password-recovery.component.scss']
})
export class PasswordRecoveryComponent {

  email: String;
  password_2: String;
  password: String;
  id: String;

  constructor(
      private requestService: RequestService,
    	private alertService: AlertService,
      private route: ActivatedRoute,
      private router: Router
    ) {
      this.route.params.subscribe( params => {
        this.id = params['id'];
      });
    }

    recovery(){
      if(this.email){
        this.requestService.request('auth/recover', {
          email: this.email
        }, (data) => {
          this.alertService.success('mail inviata a ' + this.email + '.');
        }, (error) => {
          this.alertService.error(error.error.text);
        }
      );
    }
    else if(this.password==this.password_2){
      this.requestService.request('auth/change_password', {
        id: this.id,
        password: this.password
      }, (data) => {
        this.router.navigateByUrl('/login');
        this.alertService.success(data);
      }, (error) => this.alertService.error(error.error.text));
    }
    else {
      this.alertService.error('password differenti!');
    }
  }

  back() {
    this.router.navigateByUrl('/login');
  }
}
