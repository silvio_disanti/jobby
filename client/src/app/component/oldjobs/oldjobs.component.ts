import { Component, OnInit } from '@angular/core';
import { Job } from '../../model/job';
import { UserService } from '../../service/user.service';
import { JobService } from '../../service/job.service';

@Component({
  selector: 'app-oldjobs',
  templateUrl: './oldjobs.component.html',
  styleUrls: ['./oldjobs.component.css']
})
export class OldjobsComponent implements OnInit {
   jobs: Job[] = this.jobService.onlyMyJobs();

  constructor(private jobService: JobService, private userService: UserService) { }

  ngOnInit() {
    let assignJobs = u => this.jobs = this.jobService.onlyMyJobs();

    this.userService.getObservable().subscribe(assignJobs);
    this.jobService.getObservable().subscribe(assignJobs);
  }
}
