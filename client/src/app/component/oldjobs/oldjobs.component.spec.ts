import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OldjobsComponent } from './oldjobs.component';

describe('OldjobsComponent', () => {
  let component: OldjobsComponent;
  let fixture: ComponentFixture<OldjobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OldjobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OldjobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
