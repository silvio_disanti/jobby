import { Component, OnInit } from '@angular/core';
import { Job } from '../../model/job';
import { UserService } from '../../service/user.service';
import { JobService } from '../../service/job.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
   jobs: Job[] = this.jobService.onlyCandidableJobs();

  constructor(private jobService: JobService, private userService: UserService) {

  }

  ngOnInit() {
    let assignJobs = u => this.jobs = this.jobService.onlyCandidableJobs();
    this.userService.getObservable().subscribe(assignJobs);
    this.jobService.getObservable().subscribe(assignJobs);
  }
}
