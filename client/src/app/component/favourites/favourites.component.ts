import { Component, OnInit } from '@angular/core';
import { Job } from '../../model/job';
import { UserService } from '../../service/user.service';
import { JobService } from '../../service/job.service';

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.css']
})
export class FavouritesComponent implements OnInit {
   jobs: Job[] = this.jobService.favourites();

  constructor(private jobService: JobService, private userService: UserService) {

  }

  ngOnInit() {
    let assignJobs = u => this.jobs = this.jobService.favourites();
    this.userService.getObservable().subscribe(assignJobs);
    this.jobService.getObservable().subscribe(assignJobs);
  }
}
