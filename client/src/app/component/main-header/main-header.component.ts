import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.scss']
})
export class MainHeaderComponent {

  constructor(private userService: UserService, private router: Router) { }

  logout() {
    this.userService.clearUser();
    this.router.navigateByUrl('/login');
  }

  getUseremail(){
    return this.userService.getUser().email;
  }
  
  isLogged(){
    return this.userService.isLogged();
  }
}
