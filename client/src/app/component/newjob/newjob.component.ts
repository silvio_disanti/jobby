import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../../service/user.service';
import { AlertService } from '../../service/alert.service';
import { Router } from '@angular/router';
import { JobService } from '../../service/job.service';

import { Job } from '../../model/job';
import { AddressInformation } from '../../model/address_information';

@Component({
  selector: 'app-newjob',
  templateUrl: './newjob.component.html',
  styleUrls: ['./newjob.component.scss']
})
export class NewJobComponent {
   genderTree: String[] = ['Home', 'Consulenza', 'Lezioni', 'Altro'];
   job: Job = new Job();
   countryTree: String[] = ['IT', 'FR', 'GB'];

  constructor(
    private userService: UserService,
    private jobService: JobService,
    private router: Router,
    private alertService: AlertService) {
      this.job.address_information = new AddressInformation();
    }

    back(){
      this.router.navigateByUrl('/home');
    }

    newJob() {
      this.jobService.addJob(this.job);
      this.router.navigateByUrl('/home');
    }
}
