import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Job } from '../../model/job';
import { User } from '../../model/user';
import { Utils } from '../../utils';

import { UserService } from '../../service/user.service';
import { JobService } from '../../service/job.service';

@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.scss']
})
export class JobComponent {
  @Input() job: Job;

  constructor(
    private userService: UserService,
    private jobService: JobService,
    private router: Router) {
	}

  getImagePath(){
    return this.userService.getImagePath(this.job.created_by)
  }

  getUserEmail(){
    return this.userService.getUser().email;
  }

  isCandidate(): boolean {
    return this.userService.isLogged() && Utils.Contains(this.job.candidates, this.userService.getUser());
  }

  isFavourite(){
    return this.userService.isLogged() && this.userService.getUser().favourites.includes(this.job.id);
  }

  canAddToFav() {
    return this.userService.isLogged() && this.job.created_by.email !== this.userService.getUser().email;
  }

  canAssign() {
    return this.userService.isLogged() &&
      this.job.created_by.email == this.userService.getUser().email &&
      this.job.IsExpirated() &&
      !this.job.assigned_to;
  }

  canCandidate() {
    return this.userService.isLogged() &&
      this.job.created_by.email != this.userService.getUser().email;
  }

  canDelete() {
    return this.userService.isLogged() &&
      this.job.created_by.email == this.userService.getUser().email &&
      !this.job.assigned_to;
  }

  addCandidate() {
    console.log('candidating');
    if (!this.isCandidate())
      this.jobService.addCandidate(this.job);
    else
      this.jobService.removeCandidate(this.job);
  }

  addFavourite(){
    if (!this.isFavourite())
      this.userService.addFavourite(this.job);
    else
      this.userService.removeFavourite(this.job);
  }

  getAddress() {
    return Utils.AddressToString(this.job.address_information);
  }

  assignJob(candidate: User){
    this.jobService.assign(this.job, candidate);
  }

  endJob() {
    this.jobService.end(this.job);
  }

  sendVatGov(){
    this.jobService.sendVatGov(this.job);
  }

  leaveComment() {
    if (this.userService.getUser().email == this.job.created_by.email)
      this.router.navigateByUrl('/comment/' + this.job.assigned_to.email + '/' + this.job.id);
    else
      this.router.navigateByUrl('/comment/' + this.job.created_by.email + '/' + this.job.id);
  }

  deleteJob() {
    this.jobService.deleteJob(this.job);
  }

  canLeaveComment() {
    if (this.userService.getUser().email == this.job.created_by.email) {
      return !this.job.assigned_to.feedbacks.some(fb => fb.job == this.job.id);
    }
    else
      return !this.job.created_by.feedbacks.some(fb => fb.job == this.job.id);
  }
  
  canMakeInvoice(): Boolean {
    return this.userService.getUser().vat &&
      this.job.assigned_to.email == this.userService.getUser().email &&
      this.job.ended && !this.job.invoiceDone;
  }
}
