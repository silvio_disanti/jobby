export class AddressInformation {
  address: String;
  house_numbering: String;
  city: String;
  province: String;
  zipcode: Number;
  country: String;
}
