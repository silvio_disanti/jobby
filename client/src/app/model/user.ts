import { Job } from './job';
import { Feedback } from './feedback';
import { AddressInformation } from './address_information';

export class User {
  username: String;
  email: string;
  profile_image: String;
  phone_number: String;
  vat: String;
  favourites: String[] = []; //sono gli id dei job
  ratingNumber: Number = 0;
  feedbacks: Feedback[] = [];//sono gli id dei feedback
  address_information: AddressInformation= new AddressInformation();

  getRating() {
    let avgComm: number = 0;
    let avgPunc: number = 0;
    let avgProf: number = 0;
    this.feedbacks.forEach(f => {
      avgComm += f.communication;
      avgProf += f.professionalism;
      avgPunc += f.punctuality;
    });
    avgComm /= this.feedbacks.length;
    avgPunc /= this.feedbacks.length;
    avgProf /= this.feedbacks.length;
    return Math.round( (avgComm + avgPunc + avgProf)/3.0 * 10) / 10;
  }
}
