import { Job } from '../model/job';
export class Feedback {
  writer: String;
  comment: String;
  punctuality: number = 0;
  professionalism: number = 0;
  communication: number = 0;
  job: String;
}
