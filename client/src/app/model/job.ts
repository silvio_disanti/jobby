import { User } from '../model/user';
import { AddressInformation } from './address_information';

export class Job {
  id: String;
  title: String;
  description: String;
  expiration_date: Date;
  gender: String;
  created_date: Date = new Date();
  candidates: User[] = [];
  assigned_to: User;
  created_by: User;
  price: Number;
  ended: Boolean;
  invoiceDone: Boolean;
  address_information: AddressInformation;

  private static now = Date.now();

  GetPassedTime() {
    return Job.now - this.created_date.valueOf();
  }

  GetTotalTime() {
    return this.expiration_date.valueOf() - this.created_date.valueOf();
  }

  IsExpirated(): Boolean {
    return this.GetPassedTime() >= this.GetTotalTime();
  }

}
