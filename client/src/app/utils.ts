import { Job } from './model/job';
import { User } from './model/user';
import { Feedback } from './model/feedback';
import { AddressInformation } from './model/address_information';

export class Utils {

  static userFromJSON(json): User {
    let user = new User();
    user.username = json.username;
    user.email = json.email;
    user.phone_number = json.phone_number;
    user.vat = json.vat;
    user.favourites = json.favourites;
    user.profile_image = json.profile_image;
    json.feedbacks.forEach(fb => {
      user.feedbacks.push(this.feedbackFromJSON(fb));
    });
    user.address_information = json.address_information ?
      this.addressFromJSON(json.address_information):
      new AddressInformation();
    return user;
  }

  static jobFromJSON(json, loadCandidates = true): Job {
    let job = new Job();
    job.id = json._id;
    job.title = json.title;
    job.created_by = this.userFromJSON(json.created_by);
    job.description = json.description;
    job.created_date = new Date(json.created_date);
    job.expiration_date = new Date(json.expiration_date);
    job.gender = json.gender;
    job.price = json.price;
    job.candidates = [];
    if (loadCandidates) {
      json.candidates.forEach(user => {
        job.candidates.push(this.userFromJSON(user));
      });
    }
    job.assigned_to = json.assigned_to ? this.userFromJSON(json.assigned_to) : undefined;
    job.ended = json.ended || false;
    job.invoiceDone = json.invoiceDone || false;
    job.address_information = json.address_information ?
      this.addressFromJSON(json.address_information):
      new AddressInformation();

    return job;
  }

  static addressFromJSON(json): AddressInformation {
    let address_information = new AddressInformation();
    address_information.address = json.address;
    address_information.house_numbering = json.house_numbering;
    address_information.city = json.city;
    address_information.province = json.province;
    address_information.zipcode = json.zipcode;
    address_information.country = json.country;
    return address_information;
  }

  static feedbackFromJSON(json): Feedback {
    let feedback = new Feedback();
    feedback.writer = json.writer;
    feedback.comment = json.comment;
    feedback.punctuality = json.punctuality;
    feedback.professionalism = json.professionalism;
    feedback.communication = json.communication;
    feedback.job = json.job;
    return feedback;
  }

  static Contains(list, item): boolean {
    if (item instanceof User)
      return list.some(u => u.email === item.email);
    else if (item instanceof Job)
      return list.some(j => j.id === item.id);
    else return list.includes(item);
  }

  static AddressToString(address: AddressInformation): String {
    return (address.address ? address.address + ', ' : '') +
      (address.house_numbering ? address.house_numbering + ', ' : '') +
      (address.city ? address.city : '') +
      (address.province ? ' (' + address.province + ')': '');
  }
}
