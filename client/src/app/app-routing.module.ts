import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './component/home/home.component';
import { LoginComponent } from './component/login/login.component';
import { RegisterComponent } from './component/register/register.component';
import { ProfileComponent } from './component/profile/profile.component';
import { NewJobComponent } from './component/newjob/newjob.component';
import { OldjobsComponent } from './component/oldjobs/oldjobs.component';
import { HelpComponent } from './component/help/help.component';
import { FavouritesComponent } from './component/favourites/favourites.component';
import { VerifyComponent } from './component/verify/verify.component';
import { AssignedToMeComponent } from './component/assigned-to-me/assigned-to-me.component';
import { RateandCommentComponent } from './component/rateand-comment/rateand-comment.component';
import { PasswordRecoveryComponent } from './component/password-recovery/password-recovery.component';
import { TermsComponent } from './component/terms/terms.component';

import { LoggedGuard } from './service/guard/logged.service';
import { NotLoggedGuard } from './service/guard/not-logged.service';

const appRoutes: Routes = [
	//all the path goes here
	{ path: 'home', component: HomeComponent, canActivate: [NotLoggedGuard] },
	{ path: 'terms', component: TermsComponent},
	{ path: 'login', component: LoginComponent, canActivate: [LoggedGuard] },
	{ path: 'verify', component: VerifyComponent, canActivate: [LoggedGuard] },
	{ path: 'verify/:id', component: VerifyComponent, canActivate: [LoggedGuard] },
	{ path: 'register', component: RegisterComponent},
	{ path: 'profile/:mail', component: ProfileComponent, canActivate: [NotLoggedGuard] },
	{ path: 'newjob', component: NewJobComponent, canActivate: [NotLoggedGuard] },
	{ path: 'oldjobs', component: OldjobsComponent, canActivate: [NotLoggedGuard] },
	{ path: 'favourites', component: FavouritesComponent, canActivate: [NotLoggedGuard] },
	{ path: 'comment/:mail/:job', component: RateandCommentComponent, canActivate: [NotLoggedGuard] },
	{ path: 'password-recovery', component: PasswordRecoveryComponent/*, canActivate: [LoggedGuard] */},
	{ path: 'password-recovery/:id', component: PasswordRecoveryComponent/*, canActivate: [LoggedGuard]*/ },
	{ path: 'assigned-to-me', component: AssignedToMeComponent },
	{ path: 'help', component: HelpComponent },
	{ path: '**', component: HomeComponent, canActivate: [NotLoggedGuard] }
	];

@NgModule({
  imports: [
	  RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
