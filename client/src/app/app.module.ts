import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { StarRatingModule } from 'angular-star-rating';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatInputModule} from '@angular/material';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {MatListModule} from '@angular/material/list';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressBarModule} from '@angular/material/progress-bar';

import { AppComponent } from './component/app/app.component';
import { HomeComponent } from './component/home/home.component';
import { LoginComponent } from './component/login/login.component';
import { RegisterComponent } from './component/register/register.component';
import { JobComponent } from './component/job/job.component';
import { ProfileComponent } from './component/profile/profile.component';
import { NewJobComponent } from './component/newjob/newjob.component';
import { FavouritesComponent } from './component/favourites/favourites.component';
import { HelpComponent } from './component/help/help.component';
import { OldjobsComponent } from './component/oldjobs/oldjobs.component';
import { AlertComponent } from './component/alert/alert.component';

import { UserService } from './service/user.service';
import { JobService } from './service/job.service';
import { RequestService } from './service/request.service';
import { AlertService } from './service/alert.service';

import {ErrorStateMatcher} from '@angular/material/core';

import { VerifyComponent } from './component/verify/verify.component';
import { RateandCommentComponent } from './component/rateand-comment/rateand-comment.component';
import { AssignedToMeComponent } from './component/assigned-to-me/assigned-to-me.component';
import { PasswordRecoveryComponent } from './component/password-recovery/password-recovery.component';
import { MainHeaderComponent } from './component/main-header/main-header.component';
import { TermsComponent } from './component/terms/terms.component';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonToggleModule,
    MatToolbarModule,
    MatCardModule,
    MatListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatGridListModule,
    MatSlideToggleModule,
    MatIconModule,
    MatProgressBarModule,
    StarRatingModule.forRoot()
  ],
  providers:[
    UserService,
    JobService,
    RequestService,
    AlertService,
    ErrorStateMatcher
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    JobComponent,
    ProfileComponent,
    NewJobComponent,
    HomeComponent,
    FavouritesComponent,
    HelpComponent,
    OldjobsComponent,
    AlertComponent,
    VerifyComponent,
    RateandCommentComponent,
    AssignedToMeComponent,
    PasswordRecoveryComponent,
    MainHeaderComponent,
    TermsComponent
  ],
  bootstrap: [ AppComponent ],
})

export class AppModule {}
