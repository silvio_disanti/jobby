import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as rx from 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  //private serverUrl = 'https://yourServerAddress.domain';
  private serverUrl = 'https://localhost:4000';

  constructor(private http: HttpClient) { }

  request(requestUrl: String, parameters: any, callback: (a: any)=>void, errorCallback: (e: any)=>void = undefined) {
    if (requestUrl.charAt(0) != '/') requestUrl = '/' + requestUrl;
    this.http.post(this.serverUrl + requestUrl, parameters)
    .catch((error: any, caught) => {
      if (errorCallback) errorCallback(error);
      return rx.Observable.throw(error);
    }).subscribe(data => {
      callback(data);
    });
  }
  
  getServerUrl(){
    return this.serverUrl;
  }
}
