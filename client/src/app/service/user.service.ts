import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { Job } from '../model/job';
import { Observable, Subject } from 'rxjs';
import { Utils } from '../utils';

import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private user: User;
  private tmpuser: User;
  private userSubject = new Subject<User>();
  constructor(private requestService: RequestService) {
  	if (this.user == null && localStorage['usertoken'] != null) {
  		this.setUser();
  	}
  }

  getObservable() { return this.userSubject.asObservable(); }

  setUser() {
    this.requestService.request('user/getUser',
      { token: localStorage['usertoken'] },
      res => {
        this.user = Utils.userFromJSON(res.message)
        this.userSubject.next(this.user);
      },
      err => {
        if (err.status == 401) { //not authorized
          this.clearUser();
        }
      }
    );
  }

  getSpecificUser(email: String, cb: (User) => void){
    this.requestService.request('user/getUser', { email: email },
      res => cb(Utils.userFromJSON(res.user)));
  }

  getUser() {
    return this.user || new User();
  }

  clearUser() {
    localStorage.removeItem('usertoken');
    this.user = null;
    this.userSubject.next(new User());
  }

  isLogged(): boolean {
    return localStorage['usertoken'];
  }

  addFavourite(job: Job){
    this.requestService.request('user/addFavourite', {
        token: localStorage['usertoken'],
        data: JSON.parse(JSON.stringify(job))
      }, res => this.user.favourites.push(job.id));
  }
  removeFavourite(job: Job){
    this.requestService.request('user/remFavourite', {
        token: localStorage['usertoken'],
        data: JSON.parse(JSON.stringify(job.id))
      }, res => this.user.favourites.splice( this.user.favourites.indexOf(job.id), 1 ));
  }

  getImagePath(user: User = this.user) {
    return user.profile_image ?
      this.requestService.getServerUrl() + '/' + user.profile_image :
      "assets/images/icons/profile.svg";
  }
}
