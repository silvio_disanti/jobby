import { Injectable } from '@angular/core';
import { Job } from '../model/job';
import { User } from '../model/user';
import { Observable, Subject} from 'rxjs';

import { UserService } from './user.service';
import { RequestService } from './request.service';
import { Utils } from '../utils';

@Injectable({
  providedIn: 'root'
})
export class JobService {
  private jobs: Job[] = [];
  private jobsSubject = new Subject<Job[]>();

  constructor(private userService: UserService,
              private requestService: RequestService) {
    this.requestJobs(requestService);
    setInterval(() => {this.requestJobs(requestService)}, 30000);
  }

  private requestJobs(reqSer: RequestService) {
    reqSer.request('job/getJobs', null, res => {
      this.jobs = [];
      res.message.forEach(element => {
        this.jobs.push(Utils.jobFromJSON(element));
      });
      this.jobsSubject.next(this.jobs);
    });
  }

  getObservable() { return this.jobsSubject.asObservable(); }

  getJobs() {
    return this.jobs;
  }

  getJob(id: String) {
    return this.jobs.find(j => j.id === id);
  }

  excludeUser(user = this.userService.getUser()) {
    return this.filter((job) => job.created_by.email != (user ? user.email : ''));
  }

  onlyUser(user = this.userService.getUser()) {
    return this.filter((job) => job.created_by.email == (user ? user.email : ''));
  }

  onlyCandidableJobs(user = this.userService.getUser()) {
    return this.filter(job => job.created_by.email != (user ? user.email : "") && !job.assigned_to);
  }

  onlyMyJobs(user = this.userService.getUser()) {
    return this.filter(job => job.created_by.email == (user ? user.email : ""));
  }

  onlyMyAssignedJobs(user = this.userService.getUser()) {
    return this.filter(job => (job.assigned_to && user) ? job.assigned_to.email == user.email : false);
  }

  favourites(user = this.userService.getUser()) {
    return this.filter(job => user ? user.favourites.includes(job.id) : false);
  }

  private filter(strategy: (job: Job) => boolean){
    return this.jobs.filter(job => strategy(job));
  }

  addCandidate(job: Job){
    if (!Utils.Contains(job.candidates, this.userService.getUser())) {
      this.requestService.request('job/addCandidate', {
          token: localStorage['usertoken'],
          data: JSON.parse(JSON.stringify(job))
        }, res => job.candidates.push(this.userService.getUser()));
    }
  }

  removeCandidate(job: Job){
    if (Utils.Contains(job.candidates, this.userService.getUser())) {
      this.requestService.request('job/remCandidate', {
          token: localStorage['usertoken'],
          data: JSON.parse(JSON.stringify(job))
        }, res => {
          this.jobs.splice(this.jobs.indexOf(job));
          var candidates = job.candidates;
          candidates.splice(job.candidates.indexOf(this.userService.getUser()));

          var job_tmp = job;
          job_tmp.candidates = candidates;

          this.jobs.push(job_tmp);
          console.log("Removed candidature");
        });
    }
  }

  assign(job: Job, candidate: User){
    this.requestService.request('job/assign',{
      token: localStorage['usertoken'],
      jobID: job.id,
      userMail: candidate.email
    }, res => job.assigned_to = candidate);
  }

  end(job: Job){
    this.requestService.request('job/end',{
      token: localStorage['usertoken'],
      jobID: job.id
    }, res => job.ended = true);
  }

  deleteJob(job: Job) {
    this.requestService.request('job/deleteJob', {
      token: localStorage['usertoken'],
      jobID: job.id
    },
    res => {
      this.jobs.splice(this.jobs.indexOf(job));
      this.jobsSubject.next(this.jobs);
    });
  }

  sendVatGov(job: Job){
    console.log(job);
    this.requestService.request('vat/new', {
      token: localStorage['usertoken'],
      j: job
    }, res => console.log(res));
  }

  addJob(job: Job) {
      job.created_by = this.userService.getUser();
      this.requestService.request('job/newjob', {
        token: localStorage['usertoken'],
        data: JSON.parse(JSON.stringify(job))
      }, (data) => this.jobs.push(job),
    (err)=> {

    });
  }
}
