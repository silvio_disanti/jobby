import { TestBed, inject } from '@angular/core/testing';

import { NotLoggedGuard } from './not-logged.service';

describe('NotLoggedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotLoggedGuard]
    });
  });

  it('should be created', inject([NotLoggedGuard], (service: NotLoggedGuard) => {
    expect(service).toBeTruthy();
  }));
});
