import { TestBed, inject } from '@angular/core/testing';

import { LoggedGuard } from './logged.service';

describe('LoggedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoggedGuard]
    });
  });

  it('should be created', inject([LoggedGuard], (service: LoggedGuard) => {
    expect(service).toBeTruthy();
  }));
});
