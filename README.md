# Jobby

Jobby is a web application developed on MEAN stack as an easy way to search/offer jobs.

## Getting started

These instruction will get you a copy of the project up and running on your local machine for development and testing purpose.

### Prerequisites

If not present in your machine you will need to install [Node](https://nodejs.org) runtime and [MongoDB](https://www.mongodb.com/download-center/community) server. You will also need to generate a [certificate](https://letsencrypt.org/docs/certificates-for-localhost/) for your machine. Once you are done with Node, install the angular-cli via `npm`

```
npm install -g @angular/cli
```
And you're done!  
*Notice: we used [NGingx](https://www.nginx.com/resources/wiki/) as reverse proxy to serve our pages.*

### Installing

After cloning the repo there are other few steps to see Jobby up and running.
Put your certificate in an appropriate folder in the root of client and server. For server-side use the certificate's absolute path and put it in the options const in `jobby/server/bin/www`. For client-side you have to add in `angular.json` under the "serve"

```
"options": {
    "sslKey": "path/to/localhost.key",
    "sslCert": "path/to/localhost.crt"
}
```
Last thing you have to do is to set up the e-mail sender, open `jobby/server/api/routes/sendMail.js` and configure it

```
service: 'the service you're using,
  auth: {
    user: 'username@service.domain',
    pass: 'YourPassw0rd'
  }
```
Once you are done with this

```
cd client
npm install
cd ..
cd server
npm install
```
Almost done, **start a mongodb service** and use two separate shell to start client and server with

```
npm start
```
Open your browser at https://localhost:3001 and here we are, welcome to Jobby!

### Authors

* **Rrok Gjinaj** - rrok.gjinaj@studio.unibo.it
* **Loris Cangini** - loris.cangini@studio.unibo.it
* **Silvio Di Santi** - silvio.disanti@studio.unibo.it