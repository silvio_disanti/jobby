var mongoose = require('mongoose');
var user = require('./user');
var job = require('./job');
var Schema = mongoose.Schema;

var FeedbackSchema = new Schema({
  _id: Schema.Types.ObjectId,
  writer: {
    type: String,
    required: true
  },
  job: {
    type: Schema.Types.ObjectId, ref: 'Job',
    required: true
  },
  communication: {
	  type: Number,
    default: 0
  },
  punctuality: {
	  type: Number,
    default: 0
  },
  professionalism: {
	  type: Number,
    default: 0
  },
  comment: String
});

module.exports = mongoose.model('Feedback', FeedbackSchema);
