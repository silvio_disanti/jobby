var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var address_information = require('./address_information');
var feedback = require('./feedback');
var Schema = mongoose.Schema;

var UserSchema = new mongoose.Schema({
  _id: Schema.Types.ObjectId,
  username: {
    type: String,
    required: true
  },
	password: {
    type: String,
    required: true
  },
  phone_number: {
    type: Number,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  vat: {
    type: String,
    required: false
  },
  invoice_number: {type: Number, default: 0},
  vat: String,
  profile_image: String,
  favourites : [ {type: Schema.Types.ObjectId, ref: 'Job' } ],
  verified: { type: Boolean, default: false },
  feedbacks: [ {type: Schema.Types.ObjectId, ref: 'Feedback'} ],
  address_information: {type: Schema.Types.ObjectId, ref: 'AddressInformation'}
});

UserSchema.pre('save', function (next) {
    var user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, null, function (err, hash) {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

UserSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('User', UserSchema);
