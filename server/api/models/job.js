var mongoose = require('mongoose');
var user = require('./user');
var address_information = require('./address_information');
var Schema = mongoose.Schema;
var JobSchema = new Schema({
  _id: Schema.Types.ObjectId,
  created_by: {type: Schema.Types.ObjectId, ref: 'User' },
  title: {
    type: String,
    unique: false,
    required: true
    },
  description: {
    type: String,
    required: true
  },
  created_date: {
    type: Date
  },
  expiration_date: {
    type: Date
  },
  gender: String,
  price: Number,
  ended: {type: Boolean, default: false},
  invoiceDone: {type: Boolean, default: false},
  candidates : [ {type: Schema.Types.ObjectId, ref: 'User' } ],
  assigned_to: {type: Schema.Types.ObjectId, ref: 'User' },
  address_information: {type: Schema.Types.ObjectId, ref: 'AddressInformation'}
});

module.exports = mongoose.model('Job', JobSchema);
