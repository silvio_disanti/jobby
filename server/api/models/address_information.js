var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AddressInformationSchema = new Schema({
  _id: Schema.Types.ObjectId,
  address: String,
  house_numbering: String,
  city: String,
  province: String,
  zipcode: Number,
  country: String
});

module.exports = mongoose.model('AddressInformation', AddressInformationSchema);
