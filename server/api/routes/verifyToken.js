var jwt = require('jsonwebtoken');
var config = require('../config/database');

var User = require("../models/user");

module.exports = function(req, res, callback) {
  var token = req.body.token || req.cookies.token || req.query.token;
  if (!token) {
    res.status(401).send('Unauthorized Request. JWT Token not found.');
  }
  try {
    var userId = jwt.verify(req.body.token, config.secret);
    var user = User.findById(userId._id)
      .populate('address_information')
      .populate('feedbacks')
      .exec((err, user) => {
        if(err || !user) res.status(401).send('Unauthorized Request. Invalid JWT Token.');
        else callback(user);
      });
  } catch(err) {
    res.status(401).send('Unauthorized Request. Invalid JWT Token.');
  }
}
