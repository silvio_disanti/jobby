var mongoose = require('mongoose');
var passport = require('passport');
require('../config/passport')(passport);
var express = require('express');
var router = express.Router();


//useful statuses
//200 = OK
//201 = Created
//400 = Bad Request
//401 = Unauthorized - The request requires user authentication
//403 = Forbidden - The server understood the request, but is refusing to fulfill it. Authorization will not help.
//404 = Not Found
//500 = Internal Server Error

//GET home page.
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Jobbyfi' });
});

router.use('/auth', require('./authentication'));
router.use('/user', require('./user'));
router.use('/job', require('./job'));
router.use('/vat', require('./vat'));

module.exports = router;
