var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var verifyToken = require("./verifyToken");

var Job = require("../models/job");
var User = require("../models/user");
var AddressInformation = require("../models/address_information");
var Feedback = require("../models/feedback");

router.post('/newjob', function(req, res) {
  console.log(req.body.data);

  var user = verifyToken(req, res, function(user) {
    if(!req.body.data.title ||
      !req.body.data.description ||
      !req.body.data.expiration_date ||
      !req.body.data.gender ||
      !req.body.data.price)
    {
      res.status(400).json('Please compile entire form with needed data.');
    }
    else {
       User.findOne({
        email: req.body.data.created_by.email
       },  function(err, user) {
        if (err) throw err;

        var address = new AddressInformation({
          _id: mongoose.Types.ObjectId(),
          address: req.body.data.address_information.address,
          house_numbering: req.body.data.address_information.house_numbering,
          city: req.body.data.address_information.city,
          province: req.body.data.address_information.province,
          zipcode: req.body.data.address_information.zipcode,
          country: req.body.data.address_information.country
        });
        address.save();

        new Job ({
          _id: mongoose.Types.ObjectId(),
          title: req.body.data.title,
          description: req.body.data.description,
          expiration_date: req.body.data.expiration_date,
          created_date: Date.now(),
          gender : req.body.data.gender,
          price: req.body.data.price,
          created_by: user._id,
          address_information: address._id
        }).save(function(err) {
          if (err)
            res.status(400).json({success: false, msg: err});
          else
            res.status(201).json({ok: true, message: 'Successfully created new job.'});
        });
       });
    }
  });
});

//job displaying in home
router.post('/getJobs',function(req,res){
    Job.find()
      .populate({
         path: 'created_by',
         populate: [{
           path: 'address_information',
           model: 'AddressInformation'
         }, {
           path: 'feedbacks',
           model: 'Feedback'
         }]
      })
      .populate('candidates')
      .populate('address_information')
      .populate({
         path: 'assigned_to',
         populate: [{
           path: 'address_information',
           model: 'AddressInformation'
         }, {
           path: 'feedbacks',
           model: 'Feedback'
         }]
      })
      .exec((err, jobs) => {
        res.json({ok: true, message: jobs})
      });
});

router.post('/addCandidate', function(req, res){
	verifyToken(req, res, user => {
    User.findOne({
      email: req.body.data.created_by.email
    }, (err, created_by) => {
      Job.findOneAndUpdate({
          created_date: req.body.data.created_date,
          created_by: created_by._id
        },
        { $addToSet: { candidates: user._id } },
        (err, job) => {
          if (err) res.status(500).json("User cannot be added to candidates.");
          else {
            User.findById(job.created_by, (err, creator) => {
              require('./sendMail')(
                creator.email,
                '[Jobby] Nuovo candidato per la tua offerta',
                user.username + ' si è candidato per la tua offerta: ' + job.title + '.'
              );
            });
            res.json({ok: true, message: "Candidate added to this job."});
          }
        }
      );
    });
  });
});

router.post('/remCandidate', function(req, res){
	verifyToken(req, res, u => {
    Job.findByIdAndUpdate(req.body.data.id, { $pull: { candidates: u._id }}, function (err, user) {
      if (err) res.status(500).send("Candidate cannot be removed from this job.");
      else {
      res.json({ok: true, message: "Candidate removed from this job."});
      }
    });
  });
});

router.post('/assign', function(req, res){
	verifyToken(req, res, jobOwner => {
    Job.findById(req.body.jobID, (err, job) => {
      if (err || !job) res.status(500).json("The job cannot be found.");
      else if (job.created_by.toString() != jobOwner._id.toString()) res.status(400).json("You can't assign someone else's job!");
      else if (job.assigned_to) res.status(400).json("You can't assign an already assigned job!");
      else User.findOne({email: req.body.userMail}, (err, user) => {
        if (err) res.status(500).json("The assigned user cannot be found.");
        else {
          job.assigned_to = user._id;
          job.save();
          res.json({ok: true, message: "Job assigned to " + user.username + "."});
        }
      });
    });
  });
});

router.post('/end', function(req, res){
	verifyToken(req, res, jobOwner => {
    Job.findById(req.body.jobID, (err, job) => {
      if (err) res.status(500).json("The job cannot be found.");
      else if (job.created_by.toString() != jobOwner._id.toString()) res.status(500).json("You can't end someone else's job!");
      else if (!job.assigned_to) res.status(400).json("You can't end an unassigned job!");
      else if (job.ended) res.status(400).json("You can't end an already ended job!");
      else {
        job.ended = true;
        job.save();
        res.json({ok: true, message: "Job ended."});
      }
    });
  });
});

router.post('/deleteJob', function(req, res) {
  verifyToken(req, res, jobOwner => {
    Job.findById(req.body.jobID, (err, job) => {
      if (err) res.status(500).json("The job cannot be found.");
      else if (job.created_by.toString() != jobOwner._id.toString()) res.status(500).json("You can't delete someone else job!");
      else {
        Job.deleteOne({_id: req.body.jobID}, (err, job) => {
          if (err) throw err;
        });
        res.json({ok: true, message: "Job deleted successfully."});
      }
    });
  });
});

module.exports = router;
