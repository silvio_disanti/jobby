var express = require('express');
var router = express.Router();
var multer = require('multer');
var mongoose = require('mongoose');

var User = require("../models/user");
var Job = require("../models/job");
var AddressInformation = require("../models/address_information");
var Feedback = require("../models/feedback");

var verifyToken = require("./verifyToken");

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, 'public/media/profile_images/');
  },
  filename: function(req, file, cb) {
    User.findOne({email: file.originalname}, (err, user) => {
      cb(null, user._id + '.jpg');
    });
  }
});

router.post('/getUser', function(req, res) {
  if (req.body.token)
    verifyToken(req, res, user => res.json({ok: true, message: user}));
  else if (req.body.email)
    User.findOne({email: req.body.email})
      .populate('address_information')
      .populate('feedbacks')
      .exec((err, user) => {
      res.json({ok: true, user: user});
    });
  else
    res.status(400).send('Provide needed information to find an user.');
});

router.post('/addFavourite', function(req, res){
	verifyToken(req, res, u => {
    User.findOne({
      email: req.body.data.created_by.email
    }, (err, user) => {
      Job.findOne(
        {
          created_date: req.body.data.created_date,
          created_by: user._id
        }, function(err, job) {
    		 	if (err) throw err;

          User.findByIdAndUpdate(u._id, { $addToSet: { favourites: job._id }}, function (err, user) {
            if (err) res.status(500).send("Job cannot be added to favourites.");
            res.json({ok: true, message: "Job added to favourites."});
          });
        }
      );
    });
  });
});

router.post('/remFavourite', function(req, res){
	verifyToken(req, res, u => {
    User.findByIdAndUpdate(u._id, { $pull: { favourites: req.body.data }}, function (err, user) {
      if (err) res.status(500).send("Job cannot be removed from favourites.");
      res.json({ok: true, message: "Job removed from favourites."});
    });
  });
});

const upload = multer({ storage: storage });
router.post('/profileImage', upload.single('file'), (req, res) => {

  User.findOne({email: req.file.originalname},
    (err, user) => {
      user.profile_image = 'media/profile_images/' + user._id + '.jpg';
      user.save();
      res.json('File uploaded!');
    }
  );
});

router.post('/modifyUser', (req, res) =>{
  verifyToken(req, res, user => {
      user.username = req.body.updated_user.username;
      user.phone = req.body.updated_user.phone;
      if(req.body.updatePassword){
        user.password = req.body.updatePassword;
      }
      AddressInformation.findById(user.address_information, (err, address) => {
        address.country=req.body.updated_user.address_information.country;
        address.zipcode= req.body.updated_user.address_information.zipcode;
        address.province= req.body.updated_user.address_information.province;
        address.city= req.body.updated_user.address_information.city;
        address.address= req.body.updated_user.address_information.address;
        address.house_numbering= req.body.updated_user.address_information.house_numbering;
        address.save();
      });

      user.save();
      res.json('User modified.');
  });
});

router.post('/rate', function(req, res){
	verifyToken(req, res, u => {
    User.findOne({email: req.body.target}, (err, target) => {
      if (err || !target) res.status(500).send('The comment target cannot be found.');
      else {
        new Feedback ({
          _id: mongoose.Types.ObjectId(),
          writer: u.username,
          communication: req.body.communication,
          punctuality: req.body.punctuality,
          professionalism: req.body.professionalism,
          comment: req.body.comment,
          job: req.body.j
        }).save(function(err, feedback) {
          if (err)
            res.status(400).json({success: false, msg: err});
          else {
            target.feedbacks.push(feedback);
            target.save();
            res.json({ok: true, message: 'Feedback left correctly.'});
          }
        });
      }
    });
  });
});

module.exports = router;
