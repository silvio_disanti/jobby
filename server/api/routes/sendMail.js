
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
  service: '',
  auth: {
    user: '',
    pass: ''
  }
});

module.exports = function (receivers, subject, text, cb) {
  transporter.sendMail({
      from: 'no-reply@jobby',
      to: receivers,
      subject: subject,
      text: text
    }, cb);
}
