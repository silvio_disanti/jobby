var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
const request = require('request');
const axios = require('axios');
var verifyToken = require("./verifyToken");

var Job = require("../models/job");
var User = require("../models/user");

router.post('/new', function(req, res){
	verifyToken(req, res, vat_emitter => {
		axios.post('https://api.fattureincloud.it/v1/fornitori/lista', {
				api_uid: '188890',
				api_key: '98b46e7ba046f134836ca281041615ee',
				nome: vat_emitter.username
			})
			.then(function (response_fornitori) {
				axios.post('https://api.fattureincloud.it/v1/clienti/lista', {
						api_uid: '188890',
						api_key: '98b46e7ba046f134836ca281041615ee',
						nome: req.body.j.created_by.username
					})
					.then(function (response_cliente) {
							User.findByIdAndUpdate(vat_emitter._id,{
								$inc: {invoice_number: 1 }
							},function (err, user) {
								var today = new Date();
								var dd = today.getDate();
								var mm = today.getMonth()+1;
								var yyyy = today.getFullYear();
								if(dd<10)
								{
								    dd='0'+dd;
								}
								if(mm<10)
								{
								    mm='0'+mm;
								}
								var current_date  = dd+'/'+mm+'/'+yyyy;

									axios.post('https://api.fattureincloud.it/v1/fatture/nuovo', {
										api_uid: '188890',
										api_key: '98b46e7ba046f134836ca281041615ee',
										id_cliente: response_cliente.data.lista_clienti[0].id,
										id_fornitore: response_fornitori.data.lista_fornitori[0].id,
										nome: req.body.j.created_by.username,
										indirizzo_via: req.body.j.created_by.address_information.address,
										indirizzo_cap: req.body.j.created_by.address_information.zipcode,
										indirizzo_citta: req.body.j.created_by.address_information.city,
										indirizzo_provincia: req.body.j.created_by.address_information.province,
										paese_iso: req.body.j.created_by.address_information.country,
										piva: vat_emitter.vat,
										autocompila_anagrafica: false,
										salva_anagrafica: false,
										numero: user.invoice_number,
										data: current_date,
										prezzi_ivati: true,
										valuta: 'EUR',
										"lista_articoli": [
										 {
											 "id": "0",
											 "nome": req.body.j.title,
											 "quantita": 1,
											 "prezzo_netto": req.body.j.price-(req.body.j.price*0.22),
											 "prezzo_lordo": req.body.j.price,
											 "cod_iva": 22,
											 "categoria": req.body.j.gender
										 }
									 ],
									 "lista_pagamenti": [
								    {
								      "data_scadenza": current_date,
								      "importo": req.body.j.price,
								      "metodo": "not",
								      "data_saldo": current_date
								    }
								  ]
										})
										.then(function (response) {
											Job.findByIdAndUpdate(req.body.j.id, { $set: {invoiceDone: true } });
											res.json({ok: true, message: "fattura spedita."});
										})
										.catch(function (error) {
											console.log(error);
										});
							});
					})
					.catch(function (error) {
						console.log(error);
					});
			})
			.catch(function (error) {
				console.log(error);
			});
  });
});

module.exports = router;
