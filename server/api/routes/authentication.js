
var jwt = require('jsonwebtoken');
var express = require('express');
var validator = require("email-validator");
var nodemailer = require('nodemailer');
var mongoose = require('mongoose');
var router = express.Router();
var config = require('../config/database');
var User = require("../models/user");
var AddressInformation = require("../models/address_information");

const request = require('request');
const axios = require('axios');

function sendVerificationMail(receiver, id, res) {
  require("./sendMail")(
    receiver,
    '[Jobby] Conferma il tuo indirizzo email',
    'Fai click sul link per verificare il tuo indirizzo email \n https://unibo.loool.io/verify/' + id,
    (err, info) => {
        if (err) res.status(500).send('Cannot send mail.');
        else res.status(201).json({ok: true, message: 'Successfully created new user. Email confirmation needed.'})
    });
}

router.post('/register', function(req, res) {
  if (!req.body.username || !req.body.password || !req.body.email || !req.body.phone_number) {
    res.status(400).send('Please provide username, password, e-mail and phone number.');
  } else if (!validator.validate(req.body.email)) {
    res.status(400).send('Please provide a valid e-mail address.');
  } else {
    User.deleteOne({email: req.body.email, verified: false}, (err, data) => {
      var address = new AddressInformation({
        _id: mongoose.Types.ObjectId(),
        address: req.body.address,
        house_numbering: req.body.house_numbering,
        city: req.body.city,
        province: req.body.province,
        zipcode: req.body.zipcode,
        country: req.body.country
      });
      address.save();

      var newUser = new User({
        _id: mongoose.Types.ObjectId(),
        username: req.body.username,
        password: req.body.password,
        email: req.body.email,
        phone_number: req.body.phone_number,
        vat: req.body.vat,
        address_information: address._id
      });
      newUser.save((err, user) => {
        if (err) res.status(500).json(err);
        else {
          sendVerificationMail(newUser.email, newUser._id, res);
        }
      });
    });
  }
});

router.post('/verify', function(req, res) {
  User.findByIdAndUpdate(req.body.id, { $set: { verified: true }}, (err, user) => {
    if (err || !user) res.status(400).send('Verification failed.');
    else {

      let params = {
  			api_uid: '188890',
        api_key: '98b46e7ba046f134836ca281041615ee',
        nome : user.username,
        indirizzo_via: user.street_address,
        indirizzo_citta: user.city,
        indirizzo_provincia: user.state_province,
        indirizzo_cap: user.postal_zipcode,
        paese_iso: user.country,
        tel: user.phone_number,
        piva: user.vat,
        mail: user.email
      };

      axios.post('https://api.fattureincloud.it/v1/clienti/nuovo', params)
  		  .then(console.log)
        .catch(console.log);
      axios.post('https://api.fattureincloud.it/v1/fornitori/nuovo', params)
  		  .then(console.log)
        .catch(console.log);
      console.log(user.username);
      res.json(user.username);
    }
  });
});

router.post('/login', function(req, res) {
  if(!req.body.email || !req.body.password)
  {
    res.status(400).send('Please compile entire form with needed data.');
  }
  else {
    User.findOne({ email: req.body.email }, (err, user) => {
      if (err || !user) {
        res.status(401).send('Authentication failed.');
      } else {
        // check if password matches
        user.comparePassword(req.body.password, function (err, isMatch) {
          if (isMatch && !err) {
            if(user.verified) {
              var token = jwt.sign({_id: user._id}, config.secret);
              res.status(200).json({ok: true, token: token});
            } else {
              res.status(401).send('Email verification missing.');
            }
          } else {
            res.status(401).send('Authentication failed.');
          }
        });
      }
    });
  }
});

router.post('/recover', function(req, res) {
  if(!req.body.email)
  {
    res.status(400).send('Please provide an email.');
  }
  else {
    User.findOne({ email: req.body.email }, (err, user) => {
      if (err || !user) {
        res.status(401).send('User not found.');
      } else {
        require('./sendMail')(
          user.email,
          '[Jobby] Recupero password',
          'Hai ricevuto questa email perchè è stato richiesto un ripristino password per questo account: ' + user.email + '.\n' +
            'Fai click sul link per procedere al ripristino: https://unibo.loool.io/password-recovery/' + user._id + '.',
          (err, info) => res.json('email sent')
        );
      }
    });
  }
});


router.post('/change_password', function(req, res) {
  if(!req.body.id && !req.body.password)
  {
    res.status(400).send('Please provide all needed informations.');
  }
  else {
    User.findById(req.body.id, (err, user) => {
      if (err || !user) {
        res.status(401).send('User not found.');
      } else {
        user.password = req.body.password;
        user.save();
        res.json('Password changed.');
      }
    });
  }
});

module.exports = router;
